terraform {
  backend "s3" {
    bucket = "base-config-123456789"
    key    = "workspaces"
    region = "us-east-1"
  }
}